package ch.kuehle.joel.productfinder.util;

public record Tuple<F, S>(F first, S second) { }
