package ch.kuehle.joel.productfinder.util;

import ch.kuehle.joel.productfinder.model.NamedModel;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Utils {

    public static final String adminBaseRoute = "/api/admin/";
    public static final String openBaseRoute = "/api/";

    //Controller Functions
    public static ResponseEntity<Boolean> basicCRUDAction(LinkedHashMap<String, String> params, Predicate<LinkedHashMap<String, String>> serviceCall) {
        boolean success = serviceCall.test(params);
        return new ResponseEntity<Boolean>(success, success ? HttpStatus.CREATED : HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity<Boolean> basicDeleteAction(int id, Predicate<Integer> serviceCall) {
        boolean success = serviceCall.test(id);
        return new ResponseEntity<Boolean>(success, success ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    public static <M> ResponseEntity<M[]> fetchEntries(int start, int amount, BiFunction<Integer, Integer, M[]> serviceCall) {
        M[] entries = serviceCall.apply(start, amount);
        if (entries.length == 0) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<M[]>(entries, HttpStatus.OK);
    }

    public static <M> ResponseEntity<M> findEntry(String name, Function<String, Optional<M>> serviceCall) {
        Optional<M> foundCity = serviceCall.apply(name);
        return foundCity.map(modelObj -> new ResponseEntity<M>(modelObj, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }


    // Services Functions
    public static <M> List<M> getWithOffset(int start, int amount, JpaRepository<M, Integer> repo) {
        if (amount == -1)
            amount = (int)repo.count() - start;
        if (start == 0)
            return repo.findAll(PageRequest.ofSize(amount)).getContent();
        else if (start >= amount)
            return repo.findAll(PageRequest.of(1, start)).stream().limit(amount).toList();
        else {
            ArrayList<M> modelObjs = new ArrayList<M>();
            for (int i = 0; i < Math.ceil((amount + 0.01f) / start); i++) {
                modelObjs.addAll(repo.findAll(PageRequest.of(i + 1, start)).getContent());
            }
            return modelObjs.stream().limit(amount).toList();
        }
    }

    public static <M> boolean deleteEntry(int id, JpaRepository<M, Integer> repo) {
        if (repo.findById(id).isPresent()) {
            repo.deleteById(id);
            return true;
        }
        return false;
    }

    public static boolean areAnyInputsMissing(LinkedHashMap<String, String> input, String[] paramKeys) {
        if (input.isEmpty())
            return true;
        for(String paramKey : paramKeys) {
            if (!input.containsKey(paramKey) || input.get(paramKey).isEmpty())
                return true;
        }
        return false;
    }

    public static boolean areAllInputsMissing(LinkedHashMap<String, String> input, String[] paramKeys) {
        if (input.isEmpty() || !input.containsKey("id") || input.get("id").isEmpty())
            return true;
        List<String> remaining = Arrays.stream(paramKeys.clone()).collect(Collectors.toList());
        for(String paramKey : paramKeys) {
            if (!input.containsKey(paramKey) || input.get(paramKey).isEmpty())
                remaining.remove(paramKey);
        }
        return remaining.isEmpty();
    }

    public static <M extends NamedModel> Optional<M> findModelObj(String name, M exampleObj,  JpaRepository<M, Integer> repo) {
        if (name.isEmpty())
            return Optional.empty();

        ExampleMatcher nameSearchBuilder = ExampleMatcher.matchingAny().withMatcher("name", ExampleMatcher.GenericPropertyMatchers.startsWith().ignoreCase()).withIgnoreNullValues().withIgnorePaths("alternateNames", "id");

        exampleObj.setName(name);
        return repo.findOne(Example.of(exampleObj, nameSearchBuilder));
    }

    public static <M> Optional<M> tryGetEntry(LinkedHashMap<String, String> params, JpaRepository<M, Integer> repo) {
        Optional<M> dbRes;
        try {
            dbRes = repo.findById(Integer.parseInt(params.get("id")));
            return dbRes;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
}
