package ch.kuehle.joel.productfinder.controller.admin;

import ch.kuehle.joel.productfinder.model.City;
import ch.kuehle.joel.productfinder.security.Roles;
import ch.kuehle.joel.productfinder.service.CityService;
import ch.kuehle.joel.productfinder.util.Utils;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.annotation.security.RolesAllowed;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Optional;

@RestController
@AllArgsConstructor(access = AccessLevel.PUBLIC, onConstructor = @__(@Autowired))
@SecurityRequirement(name = "bearerAuth")
public class CityController {

    private final CityService service;

    private static final String baseRoute = Utils.adminBaseRoute + "city/";

    /*
     * To update the city db, use
     * curl --request POST \
     *   --url http://localhost:9090/api/admin/city/fill \
     *   --header 'Authorization: Bearer (yourToken)' \
     *   --header 'Content-Type: multipart/form-data' \
     *   --form 'file=@(absolute file path)'
     */
    @PostMapping(baseRoute + "fill")
    @RolesAllowed(Roles.Admin)
    public void fillDBFromFile(@RequestParam("file") MultipartFile dataFile, @NotNull HttpServletResponse response) {
        // Sending response before processing file to get around client timeout
		// Copyright by https://stackoverflow.com/questions/32072255/how-to-send-response-before-actions-in-spring-mvc
        try {
			PrintWriter sender = response.getWriter();
			response.setStatus(HttpServletResponse.SC_ACCEPTED);
			sender.print(response);
			sender.flush();
			sender.close();
            service.fillDB(dataFile);
            } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @PostMapping(baseRoute + "create")
    @RolesAllowed(Roles.Admin)
    public ResponseEntity<Boolean> addCity(@RequestBody LinkedHashMap<String, String> params) {
        return Utils.basicCRUDAction(params, service::addCity);
    }

    @PutMapping(baseRoute + "update")
    @RolesAllowed(Roles.Admin)
    public ResponseEntity<Boolean> updateCity(@RequestBody LinkedHashMap<String, String> params) {
        return Utils.basicCRUDAction(params, service::updateCity);
    }

    @DeleteMapping(baseRoute + "delete")
    @RolesAllowed(Roles.Admin)
    public ResponseEntity<Boolean> deleteCity(@RequestParam(name = "id") int id) {
        return Utils.basicDeleteAction(id, service::deleteCity);
    }

    @GetMapping(baseRoute + "fetch")
    @RolesAllowed(Roles.Admin)
    public ResponseEntity<City[]> fetchCities(@RequestParam(name = "start", required = false) Optional<Integer> start, @RequestParam(name = "amount", required = false) Optional<Integer> amount) {
        return Utils.fetchEntries(start.orElse(0), amount.orElse(-1), service::fetchCities);
    }

    @GetMapping(baseRoute + "search")
    @RolesAllowed(Roles.Read)
    public ResponseEntity<City> findCity(@RequestParam(name = "name") String name) {
        return Utils.findEntry(name, service::findCity);
    }
}
