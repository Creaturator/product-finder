package ch.kuehle.joel.productfinder.controller.open;

import ch.kuehle.joel.productfinder.model.Store;
import ch.kuehle.joel.productfinder.security.Roles;
import ch.kuehle.joel.productfinder.service.ProductService;
import ch.kuehle.joel.productfinder.service.StoreService;
import ch.kuehle.joel.productfinder.util.Tuple;
import ch.kuehle.joel.productfinder.util.Utils;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.annotation.security.RolesAllowed;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;

@RestController
@AllArgsConstructor(access = AccessLevel.PUBLIC, onConstructor = @__(@Autowired))
@SecurityRequirement(name = "bearerAuth")
public class ShoppingController {

    private static final String basePath = Utils.openBaseRoute + "shopping/";

    private final ProductService productService;
    private final StoreService storeService;

    @PostMapping(basePath + "optimizePrize")
    @RolesAllowed(Roles.Admin)
    public ResponseEntity<Tuple<Store, Float>> optimizePrice(@RequestBody LinkedHashMap<String, String> items) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}
