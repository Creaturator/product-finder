package ch.kuehle.joel.productfinder.controller.admin;

import ch.kuehle.joel.productfinder.model.Category;
import ch.kuehle.joel.productfinder.security.Roles;
import ch.kuehle.joel.productfinder.service.CategoryService;
import ch.kuehle.joel.productfinder.util.Utils;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.annotation.security.RolesAllowed;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.Optional;

@RestController
@AllArgsConstructor(access = AccessLevel.PUBLIC, onConstructor = @__(@Autowired))
@SecurityRequirement(name = "bearerAuth")
public class CategoryController {

    private static final String basePath = Utils.adminBaseRoute + "category/";

    private final CategoryService service;

    @PostMapping(basePath + "create")
    @RolesAllowed(Roles.Write)
    public ResponseEntity<Boolean> addCategory(@RequestBody LinkedHashMap<String, String> params) {
        return Utils.basicCRUDAction(params, service::addCategory);
    }

    @GetMapping(basePath + "fetch")
    @RolesAllowed(Roles.Admin)
    public ResponseEntity<Category[]> fetchCategories(@RequestParam(name = "start", required = false) Optional<Integer> start, @RequestParam(name = "amount", required = false) Optional<Integer> amount) {
        return Utils.fetchEntries(start.orElse(0), amount.orElse(-1), service::getCategories);
    }

    @GetMapping(basePath + "search")
    @RolesAllowed(Roles.Read)
    public ResponseEntity<Category> findCategory(@RequestParam(name = "name") String name) {
        return Utils.findEntry(name, service::findCategory);
    }

    @PutMapping(basePath + "update")
    @RolesAllowed(Roles.Write)
    public ResponseEntity<Boolean> updateCategory(@RequestBody LinkedHashMap<String, String> params) {
        // TODO: Leute konnen alle Kategorien updaten und loschen
        return Utils.basicCRUDAction(params, service::updateCategory);
    }

    @DeleteMapping(basePath + "delete")
    @RolesAllowed(Roles.Write)
    public ResponseEntity<Boolean> deleteCategory(@RequestParam(name = "id") int id) {
        return Utils.basicDeleteAction(id, service::deleteCategory);
    }

}
