package ch.kuehle.joel.productfinder.controller.open;

import ch.kuehle.joel.productfinder.model.Store;
import ch.kuehle.joel.productfinder.security.Roles;
import ch.kuehle.joel.productfinder.service.StoreService;
import ch.kuehle.joel.productfinder.util.Utils;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.annotation.security.RolesAllowed;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.Optional;

@RestController
@AllArgsConstructor(access = AccessLevel.PUBLIC, onConstructor = @__(@Autowired))
@SecurityRequirement(name = "bearerAuth")
public class StoreController {

    private static final String basePath = Utils.openBaseRoute + "store/";

    private final StoreService service;

    @PostMapping(basePath + "create")
    @RolesAllowed(Roles.Write)
    public ResponseEntity<Boolean> addStore(@RequestBody LinkedHashMap<String, String> params) {
        return Utils.basicCRUDAction(params, service::addStore);
    }

    @GetMapping(basePath + "fetch")
    @RolesAllowed(Roles.Read)
    public ResponseEntity<Store[]> fetchStores(@RequestParam(name = "start", required = false) Optional<Integer> start, @RequestParam(name = "amount", required = false) Optional<Integer> amount) {
        return Utils.fetchEntries(start.orElse(0), amount.orElse(-1), service::getStores);
    }

    @GetMapping(basePath + "search")
    @RolesAllowed(Roles.Read)
    public ResponseEntity<Store> findStore(@RequestParam(name = "name") String name) {
        return Utils.findEntry(name, service::findStore);
    }

    @PutMapping(basePath + "update")
    @RolesAllowed(Roles.Write)
    public ResponseEntity<Boolean> updateStore(@RequestBody LinkedHashMap<String, String> params) {
        // TODO: Leute konnen alle Laden updaten und loschen
        return Utils.basicCRUDAction(params, service::updateStore);
    }

    @DeleteMapping(basePath + "delete")
    @RolesAllowed(Roles.Write)
    public ResponseEntity<Boolean> deleteStore(@RequestParam(name = "id") int id) {
        return Utils.basicDeleteAction(id, service::deleteStore);
    }

}
