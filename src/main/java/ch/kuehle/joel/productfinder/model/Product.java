package ch.kuehle.joel.productfinder.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class Product extends NamedModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(name = "name")
    @NotEmpty
    @Size(max = 255)
    private String name;

    @Column(columnDefinition = "boolean default false")
    public boolean bio = false;

    @Column
    public float price;

    @Column(columnDefinition = "character", name = "nutri_score")
    @Getter(AccessLevel.PUBLIC)
    private char nutriScore = 'e';

    @ManyToOne
    @JoinColumn(name = "fk_category")
    public Category category;

    public Product(@NotEmpty @Size(max = 255) String name, float price, boolean bio, char nutriScore, Category category) {
        super(name);
        this.bio = bio;
        this.price = price;
        this.category = category;
        setName(name);
        setNutriScore(nutriScore);
    }

    @Override
    public void setName(String name) {
        this.name = name;
        super.name = name;
    }

    public void setNutriScore(char nutriScore) {
        // If the nutri score is one of a to e
        if (List.of('a', 'b', 'c', 'd', 'e').contains(nutriScore))
            this.nutriScore = nutriScore;
        else
            throw new IllegalArgumentException("Nutri score is not valid");
    }
}
