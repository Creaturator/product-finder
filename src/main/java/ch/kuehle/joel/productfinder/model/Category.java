package ch.kuehle.joel.productfinder.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class Category extends NamedModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(columnDefinition = "boolean default false")
    public boolean vegan = false;

    @Column(name = "name")
    @NotEmpty
    @Getter
    @Size(max = 255)
    private String name;

    public Category(@NotEmpty @Size(max = 255) String name, boolean vegan) {
        super(name);
        this.vegan = vegan;
        this.name = super.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
        super.name = name;
    }
}
