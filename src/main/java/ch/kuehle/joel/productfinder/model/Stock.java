package ch.kuehle.joel.productfinder.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Stock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @NotEmpty
    @ManyToOne
    @JoinColumn(name = "fk_product")
    public Product product;

    @NotEmpty
    @ManyToOne
    @JoinColumn(name = "fk_store")
    public Store store;

    @Column(columnDefinition = "integer default 0")
    public int discount;

    @Column(columnDefinition = "integer default 0", name = "confirmation_amount")
    public int confirmationAmount;

    public Stock(Product product, Store store) {
        this.product = product;
        this.store = store;
    }
}
