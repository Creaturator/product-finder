package ch.kuehle.joel.productfinder.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class Store extends NamedModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(name = "name")
    @NotEmpty
    @Getter
    @Size(max = 255)
    private String name;

    @Column
    @Size(max = 255)
    public String address;

    @ManyToOne
    @JoinColumn(name = "fk_city")
    public City city;

    public Store(@NotEmpty @Size(max = 255) String name, String address, City city) {
        super(name);
        this.name = super.name;
        this.address = address;
        this.city = city;
    }

    @Override
    public void setName(String name) {
        this.name = name;
        super.name = name;
    }
}
