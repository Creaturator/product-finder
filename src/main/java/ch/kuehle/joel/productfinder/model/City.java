package ch.kuehle.joel.productfinder.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class City extends NamedModel {

    @Id
    public int id;

    @Column(columnDefinition = "varchar(10000)", name = "alternate_names")
    @Size(max = 10000)
    public String alternateNames;

    @Column(name = "name")
    @NotEmpty
    @Size(max = 255)
    @Getter
    private String name;

    public City(int id, @NotEmpty @Size(max = 255) String name, String alternateNames) {
        super(name);
        this.name = super.name;
        this.alternateNames = alternateNames;
        this.id = id;
    }

    @Override
    public void setName(String name) {
        this.name = name;
        super.name = name;
    }
}
