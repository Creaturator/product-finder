package ch.kuehle.joel.productfinder.repository;

import ch.kuehle.joel.productfinder.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<City, Integer> {
}
