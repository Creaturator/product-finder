package ch.kuehle.joel.productfinder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class ProductfinderApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductfinderApplication.class, args);
	}

}
