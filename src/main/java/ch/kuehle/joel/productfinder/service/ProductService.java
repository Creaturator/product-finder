package ch.kuehle.joel.productfinder.service;

import ch.kuehle.joel.productfinder.model.Product;
import ch.kuehle.joel.productfinder.repository.CategoryRepository;
import ch.kuehle.joel.productfinder.repository.ProductRepository;
import ch.kuehle.joel.productfinder.util.Utils;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.Optional;

@Service
@AllArgsConstructor(access = AccessLevel.PUBLIC, onConstructor = @__(@Autowired))
public class ProductService {

    private final ProductRepository repo;
    private final CategoryRepository categoryRepo;

    public boolean addProduct(LinkedHashMap<String, String> params) {
        if (Utils.areAnyInputsMissing(params, new String[]{"name", "price", "bio", "nutriScore", "category"}))
            return false;
        repo.saveAndFlush(new Product(params.get("name"), Float.parseFloat(params.get("price")),
                Boolean.parseBoolean(params.get("bio")), params.get("nutriScore").charAt(0),
                categoryRepo.findById(Integer.parseInt(params.get("category"))).orElseThrow()));
        return true;
    }

    public Product[] getProducts(int start, int amount) {
        return Utils.getWithOffset(start, amount, repo).toArray(Product[]::new);
    }

    public Optional<Product> findProduct(String name) {
        return Utils.findModelObj(name, new Product(), repo);
    }

    public boolean updateProduct(LinkedHashMap<String, String> params) {
        if (Utils.areAllInputsMissing(params, new String[]{"name", "price", "bio", "nutriScore", "category"}))
            return false;
        Optional<Product> dbRes = Utils.tryGetEntry(params, repo);
        if (dbRes.isEmpty())
            return false;
        Product toUpdate = dbRes.get();
        try {
            if (params.containsKey("name") && !params.get("name").isEmpty())
                toUpdate.setName(params.get("name"));
            if (params.containsKey("price") && !params.get("price").isEmpty())
                toUpdate.price = Float.parseFloat(params.get("price"));
            if (params.containsKey("bio") && !params.get("bio").isEmpty())
                toUpdate.bio = Boolean.parseBoolean(params.get("bio"));
            if (params.containsKey("nutriScore") && !params.get("nutriScore").isEmpty())
                toUpdate.setNutriScore(params.get("nutriScore").charAt(0));
            if (params.containsKey("category") && !params.get("category").isEmpty())
                toUpdate.category = categoryRepo.findById(Integer.parseInt(params.get("category"))).orElseThrow();
        } catch(RuntimeException e) {
            e.printStackTrace();
            return false;
        }
        repo.saveAndFlush(toUpdate);
        return true;
    }

    public boolean deleteProduct(int id) {
        return Utils.deleteEntry(id, repo);
    }
}
