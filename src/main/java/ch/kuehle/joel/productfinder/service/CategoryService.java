package ch.kuehle.joel.productfinder.service;

import ch.kuehle.joel.productfinder.model.Category;
import ch.kuehle.joel.productfinder.repository.CategoryRepository;
import ch.kuehle.joel.productfinder.util.Utils;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.Optional;

@Service
@AllArgsConstructor(access = AccessLevel.PUBLIC, onConstructor = @__(@Autowired))
public class CategoryService {

    private final CategoryRepository repo;

    public boolean addCategory(LinkedHashMap<String, String> params) {
        if (Utils.areAnyInputsMissing(params, new String[]{"name", "vegan"}))
            return false;
        repo.saveAndFlush(new Category(params.get("name"), Boolean.parseBoolean(params.get("vegan"))));
        return true;
    }

    public Category[] getCategories(int start, int amount) {
        return Utils.getWithOffset(start, amount, repo).toArray(Category[]::new);
    }

    public Optional<Category> findCategory(String name) {
        return Utils.findModelObj(name, new Category(), repo);
    }

    public boolean updateCategory(LinkedHashMap<String, String> params) {
        if (Utils.areAllInputsMissing(params, new String[]{"name", "vegan"}))
            return false;
        Optional<Category> dbRes = Utils.tryGetEntry(params, repo);
        if (dbRes.isEmpty())
            return false;
        Category toUpdate = dbRes.get();
        try {
            if (params.containsKey("name") && !params.get("name").isEmpty())
                toUpdate.setName(params.get("name"));
            if (params.containsKey("vegan") && !params.get("vegan").isEmpty())
                toUpdate.vegan = Boolean.parseBoolean(params.get("vegan"));
        } catch(RuntimeException e) {
            e.printStackTrace();
            return false;
        }
        repo.saveAndFlush(toUpdate);
        return true;
    }

    public boolean deleteCategory(int id) {
        return Utils.deleteEntry(id, repo);
    }

}
