package ch.kuehle.joel.productfinder.service;

import ch.kuehle.joel.productfinder.model.City;
import ch.kuehle.joel.productfinder.repository.CityRepository;
import ch.kuehle.joel.productfinder.util.Utils;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Optional;

@Service
@AllArgsConstructor(access = AccessLevel.PUBLIC, onConstructor = @__(@Autowired))
public class CityService {

    private final CityRepository repo;

    public void fillDB(MultipartFile dataFile) throws IOException {
        repo.saveAllAndFlush(new BufferedReader(new InputStreamReader(dataFile.getInputStream())).lines().parallel().map(line -> {
            String[] columns = line.split("\t");
            columns = new String[]{columns[0], columns[1], columns[3]};
            return new City(Integer.parseInt(columns[0]), columns[1], columns[2]);
        }).toList());
    }

    public boolean addCity(LinkedHashMap<String, String> params) {
        if (Utils.areAnyInputsMissing(params, new String[]{"name"}))
            return false;
        repo.saveAndFlush(new City((int)repo.count() + 1, params.get("name"), params.getOrDefault("alternateNames", "")));
        return true;
    }

    public boolean updateCity(LinkedHashMap<String, String> params) {
        if (Utils.areAllInputsMissing(params, new String[]{"name", "alternateNames"}))
            return false;
        Optional<City> dbRes = Utils.tryGetEntry(params, repo);
        if (dbRes.isEmpty())
            return false;
        City toUpdate = dbRes.get();
        try {
            if (params.containsKey("name") && !params.get("name").isEmpty())
                toUpdate.setName(params.get("name"));
            if (params.containsKey("alternateNames") && !params.get("alternateNames").isEmpty())
                toUpdate.alternateNames = params.get("alternateNames");
        } catch(RuntimeException e) {
            e.printStackTrace();
            return false;
        }
        repo.saveAndFlush(toUpdate);
        return true;
    }

    public City[] fetchCities(int start, int amount) {
        return Utils.getWithOffset(start, amount, repo).toArray(City[]::new);
    }

    public Optional<City> findCity(String name) {
        if (name.isEmpty())
            return Optional.empty();

        ExampleMatcher nameSearchBuilder = ExampleMatcher.matchingAny().withMatcher("name", ExampleMatcher.GenericPropertyMatchers.startsWith().ignoreCase()).withIgnoreNullValues().withIgnorePaths("alternateNames", "id");
        ExampleMatcher altnameSearchBuilder = ExampleMatcher.matchingAny().withMatcher("alternateNames", ExampleMatcher.GenericPropertyMatchers.startsWith().ignoreCase()).withIgnoreCase().withIgnoreNullValues().withIgnorePaths("name", "id");

        City exampleCity = new City();
        exampleCity.setName(name);
        Optional<City> searchRes = repo.findOne(Example.of(exampleCity, nameSearchBuilder));
        if (searchRes.isEmpty()) {
            exampleCity = new City();
            exampleCity.alternateNames = name;
            return repo.findOne(Example.of(exampleCity, altnameSearchBuilder));
        }
        return searchRes;
    }

    public boolean deleteCity(int id) {
        return Utils.deleteEntry(id, repo);
    }

}
