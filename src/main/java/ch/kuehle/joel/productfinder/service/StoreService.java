package ch.kuehle.joel.productfinder.service;

import ch.kuehle.joel.productfinder.model.Store;
import ch.kuehle.joel.productfinder.repository.CityRepository;
import ch.kuehle.joel.productfinder.repository.StoreRepository;
import ch.kuehle.joel.productfinder.util.Utils;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.Optional;

@Service
@AllArgsConstructor(access = AccessLevel.PUBLIC, onConstructor = @__(@Autowired))
public class StoreService {

    private final StoreRepository repo;
    private final CityRepository cityRepo;

    public boolean addStore(LinkedHashMap<String, String> params) {
        if (Utils.areAnyInputsMissing(params, new String[]{"name", "address", "city"}))
            return false;
        repo.saveAndFlush(new Store(params.get("name"), params.get("address"), cityRepo.findById(Integer.parseInt(params.get("city"))).orElseThrow()));
        return true;
    }

    public Store[] getStores(int start, int amount) {
        return Utils.getWithOffset(start, amount, repo).toArray(Store[]::new);
    }

    public Optional<Store> findStore(String name) {
        return Utils.findModelObj(name, new Store(), repo);
    }

    public boolean updateStore(LinkedHashMap<String, String> params) {
        if (Utils.areAllInputsMissing(params, new String[]{"name", "address", "city"}))
            return false;
        Optional<Store> dbRes = Utils.tryGetEntry(params, repo);
        if (dbRes.isEmpty())
            return false;
        Store toUpdate = dbRes.get();
        try {
            if (params.containsKey("name"))
                toUpdate.setName(params.get("name"));
            if (params.containsKey("address"))
                toUpdate.address = params.get("address");
            if (params.containsKey("city"))
                toUpdate.city = cityRepo.findById(Integer.parseInt(params.get("city"))).orElseThrow();
        } catch(RuntimeException e) {
            e.printStackTrace();
            return false;
        }
        repo.saveAndFlush(toUpdate);
        return true;
    }

    public boolean deleteStore(int id) {
        return Utils.deleteEntry(id, repo);
    }

}
