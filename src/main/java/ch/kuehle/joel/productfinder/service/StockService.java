package ch.kuehle.joel.productfinder.service;

import ch.kuehle.joel.productfinder.model.Stock;
import ch.kuehle.joel.productfinder.repository.ProductRepository;
import ch.kuehle.joel.productfinder.repository.StockRepository;
import ch.kuehle.joel.productfinder.repository.StoreRepository;
import ch.kuehle.joel.productfinder.util.Utils;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.Optional;

@Service
@AllArgsConstructor(access = AccessLevel.PUBLIC, onConstructor = @__(@Autowired))
public class StockService {

    private final StockRepository repo;
    private final ProductRepository productRepo;
    private final StoreRepository storeRepo;

    public boolean addStock(LinkedHashMap<String, String> params) {
        if (Utils.areAnyInputsMissing(params, new String[]{"product", "store"}))
            return false;
        repo.saveAndFlush(new Stock(productRepo.findById(Integer.parseInt(params.get("product"))).orElseThrow(),
                storeRepo.findById(Integer.parseInt(params.get("store"))).orElseThrow()));
        return true;
    }

    public Stock[] getStocks(int start, int amount) {
        return Utils.getWithOffset(start, amount, repo).toArray(Stock[]::new);
    }

    public boolean updateStock(LinkedHashMap<String, String> params) {
        if (Utils.areAllInputsMissing(params, new String[]{"product", "store", "discount", "confirmationAmount"}))
            return false;
        Optional<Stock> dbRes = Utils.tryGetEntry(params, repo);
        if (dbRes.isEmpty())
            return false;
        Stock toUpdate = dbRes.get();
        try {
            if (params.containsKey("store") && !params.get("store").isEmpty())
                toUpdate.store = storeRepo.findById(Integer.parseInt(params.get("store"))).orElseThrow();
            if (params.containsKey("product") && !params.get("product").isEmpty())
                toUpdate.product = productRepo.findById(Integer.parseInt(params.get("product"))).orElseThrow();
            if(params.containsKey("discount") && !params.get("discount").isEmpty())
                toUpdate.discount = Integer.parseInt(params.get("discount"));
            if(params.containsKey("confirmationAmount") && !params.get("confirmationAmount").isEmpty())
                toUpdate.discount = Integer.parseInt(params.get("confirmationAmount"));
        } catch(RuntimeException e) {
            e.printStackTrace();
            return false;
        }
        repo.saveAndFlush(toUpdate);
        return true;
    }

    public boolean deleteStock(int id) {
        return Utils.deleteEntry(id, repo);
    }

}
