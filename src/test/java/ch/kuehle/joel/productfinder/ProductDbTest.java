package ch.kuehle.joel.productfinder;

import ch.kuehle.joel.productfinder.model.Category;
import ch.kuehle.joel.productfinder.model.Product;
import ch.kuehle.joel.productfinder.repository.CategoryRepository;
import ch.kuehle.joel.productfinder.repository.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Example;
import org.springframework.test.annotation.Rollback;
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.AUTO_CONFIGURED)
@Rollback
class ProductDbTest {

    @Autowired
    private ProductRepository testRepo;
    @Autowired
    private CategoryRepository categoryRepo;

    private final Category testCategory = new Category("Lebensmittel", false);
    private final Product testProduct = new Product("Almighurt", 0.99f, false, 'c', testCategory);

    @BeforeEach
    public void setUp() {
        categoryRepo.saveAndFlush(testCategory);
        testRepo.saveAndFlush(testProduct);
    }

    @Test
    public void testCreatingEntities() {
        Product productA = new Product("Apfel", 1.59f, true, 'a', testCategory);
        Assertions.assertNotNull(productA.getName(), "The name did not get copied to the base class.");
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Product("Birne", 1.46f, false, 'k', testCategory), "A char that is not part of the nutri score was mistakenly accepted");
        Assertions.assertDoesNotThrow(() -> new Product("Joghurt", 2.20f, true, 'c', testCategory), "A char that is part of the nutri score was mistakenly declined");
        Assertions.assertEquals('e', new Product().getNutriScore(), "The default value for nutri scores was not applied.");
        Assertions.assertFalse(new Product().bio, "The default value for the 'bio' property was not applied.");
        testRepo.save(productA);

        Product productB = new Product("Menschenfleisch", Float.MAX_VALUE, false, 'e', testCategory);
        Assertions.assertEquals(Float.MAX_VALUE, productB.price, "The default value for the 'price' property was not applied when using the max value.");
        testRepo.save(productB);

        Assertions.assertDoesNotThrow(() -> testRepo.flush(), "Flushing the data threw an unexpected error");
    }

    @Test
    public void testGetProducts() {
        Product testProduct2 = new Product("Rittersport", 1.49f, false, 'e', testCategory);
        testProduct2.bio = true;
        testRepo.saveAndFlush(testProduct2);
        Assertions.assertInstanceOf(Product.class, testRepo.findById(testProduct.id).get());
        Assertions.assertEquals(2, testRepo.count(), "The number of entries in the db was incorrect after flushing.");
        Assertions.assertArrayEquals(new Product[] {testProduct, testProduct2}, testRepo.findAll().toArray(Product[]::new), "The wrong entries were returned from the db");
        Assertions.assertTrue(testRepo.findById(testProduct2.id).get().bio, "The 'bio' property was modified in the db.");
    }

    @Test
    public void testUpdateProduct() {
        Product dbProduct = testRepo.findById(testProduct.id).get();
        dbProduct = new Product(dbProduct.getName(), dbProduct.price, dbProduct.bio, dbProduct.getNutriScore(), dbProduct.category);
        dbProduct.setNutriScore('b');
        Category mockCategory2 = new Category("Getränke", false);
        categoryRepo.saveAndFlush(mockCategory2);
        dbProduct.category = mockCategory2;
        testRepo.saveAndFlush(dbProduct);
        Product dbProduct2 = testRepo.findById(dbProduct.id).get();
        Assertions.assertNotEquals(testProduct, dbProduct2);
        Assertions.assertEquals(dbProduct2, dbProduct);
        Assertions.assertEquals(1, testRepo.findAll(Example.of(testProduct)).size());
    }

    @Test
    public void testDeleteProduct() {
        Product dbProduct = testRepo.findById(testProduct.id).get();
        testRepo.delete(dbProduct);
        Assertions.assertTrue(testRepo.findById(testProduct.id).isEmpty());
    }

}