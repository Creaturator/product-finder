package ch.kuehle.joel.productfinder;

import ch.kuehle.joel.productfinder.model.Category;
import ch.kuehle.joel.productfinder.repository.CategoryRepository;
import ch.kuehle.joel.productfinder.service.CategoryService;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.AUTO_CONFIGURED)
@Rollback
public class CategoryServiceTest {

    private CategoryService testService;
    private final Category testModel = new Category("Obst", true);

    @BeforeEach
    public void setUp() {
        testService = new CategoryService(Mockito.mock(CategoryRepository.class));
    }

    @Test
    public void testCreateCategory() {
        Assertions.assertTrue(testService.addCategory(new LinkedHashMap<>() {{put("name", testModel.getName()); put("vegan", Boolean.toString(testModel.vegan));}}), "Creating the Entity from the parameters failed");
        Category[] categories = testService.getCategories(0, 5);
        Assertions.assertEquals(1, categories.length, "More or less than one product was inserted");
        Assertions.assertEquals(testModel, categories[0], "Parsing the parameters failed");

        Assertions.assertTrue(testService.addCategory(new LinkedHashMap<>() {{put("name", testModel.getName()); put("vegan", Boolean.toString(testModel.vegan)); put("vegetarian", "always");}}), "Superfluous parameters lead to an unexpected error");

        Assertions.assertDoesNotThrow(() -> testService.addCategory(new LinkedHashMap<>() {{put("name", testModel.getName()); put("vegan", "Hello World");}}), "An unexpected error was thrown during creation");
        Assertions.assertFalse(testService.addCategory(new LinkedHashMap<>() {{put("name", testModel.getName()); put("vegan", "Hello World");}}), "The expected Exception was not handled during creation");
        Assertions.assertFalse(testService.addCategory(new LinkedHashMap<>() {{put("definitely not a name", ""); put("vegan", Boolean.toString(testModel.vegan));}}), "Creating the Entity from wrong parameters succeeded");
        Assertions.assertFalse(testService.addCategory(LinkedHashMap.newLinkedHashMap(0)), "An empty parameter list gets mistakenly accepted");
    }

    @Test
    public void testFetchEntries() {
        ArrayList<Category> categories = new ArrayList<>(15);
        for(int i = 0; i < 15; i++) {
            int iCopy = i;
            testService.addCategory(new LinkedHashMap<>() {{put("name", "Testdata " + iCopy); put("vegan", Boolean.toString(iCopy % 2 == 0));}});
            categories.add(new Category("Testdata " + iCopy, iCopy % 2 == 0));
        }
        Category[] dbRes = testService.getCategories(0, 3);
        Assertions.assertEquals(3, dbRes.length, "More or fewer elements were returned than expected");
        Assertions.assertTrue(categories.containsAll(List.of(dbRes)), "Unknown attributes were returned in fetch 1");

        Category[] dbRes2 = testService.getCategories(4, 7);
        Assertions.assertEquals(7, dbRes2.length, "More or fewer elements were returned than expected");
        Assertions.assertEquals(0, Arrays.stream(dbRes2).filter(c2 -> Arrays.asList(dbRes).contains(c2)).toArray(Category[]::new).length, "Elements were returned twice");
        Assertions.assertTrue(categories.containsAll(List.of(dbRes2)), "Unknown attributes were returned in fetch 2");

        Category[] dbRes3 = testService.getCategories(8, 5);
        Assertions.assertEquals(5, dbRes3.length, "More or fewer elements were returned than expected");
        Assertions.assertEquals(0, Arrays.stream(dbRes3).filter(c2 -> Arrays.asList(dbRes).contains(c2) || Arrays.asList(dbRes2).contains(c2)).toArray(Category[]::new).length, "Elements were returned twice");
        Assertions.assertTrue(categories.containsAll(List.of(dbRes2)), "Unknown attributes were returned in fetch 3");
    }

    @Test
    public void testUpdateCategory() {
        Assertions.assertTrue(testService.updateCategory(new LinkedHashMap<>() {{put("name", "Gemüse");}}));
        Category[] products = testService.getCategories(0, 5);
        Assertions.assertEquals(1, products.length, "A new category was inserted or the old one removed but not replaced");
        Assertions.assertNotEquals(testModel, products[0], "The model was not updated");
        Assertions.assertEquals(new Category("Gemüse", true), products[0], "Parsing the parameters failed");
        testService.deleteCategory(products[0].id);

        Assertions.assertTrue(testService.updateCategory(new LinkedHashMap<>() {{put("name", testModel.getName()); put("vegan", Boolean.toString(testModel.vegan)); put("vegetarian", "always");}}), "Superfluous parameters lead to an unexpected error");
        Assertions.assertTrue(testService.updateCategory(new LinkedHashMap<>() {{put("name", ""); put("vegan", "false");}}), "Creating the Entity from wrong parameters succeeded");
        products = testService.getCategories(0, 5);
        Assertions.assertEquals(2, products.length, "A new category was inserted or the old one removed but not replaced");
        Assertions.assertEquals(testModel, products[0], "The model was mistakenly updated");
        Assertions.assertNotEquals(testModel, products[1], "The model was not updated");
        Assertions.assertEquals(testModel.getName(), products[1].getName(), "The name has been cleared by mistake");
        Assertions.assertFalse(products[1].vegan, "The 'vegan' property was not updated");

        Assertions.assertDoesNotThrow(() -> testService.updateCategory(new LinkedHashMap<>() {{put("name", testModel.getName()); put("vegan", "Hello World");}}), "An unexpected error was thrown during creation");
        Assertions.assertFalse(testService.updateCategory(new LinkedHashMap<>() {{put("name", testModel.getName()); put("vegan", "Hello World");}}), "The expected Exception was not handled during creation");
        Assertions.assertFalse(testService.updateCategory(LinkedHashMap.newLinkedHashMap(0)), "An empty parameter list gets mistakenly accepted");
    }

    @Test
    public void testDeleteCategory() {
        Category category1 = new Category("Lebensmittel", false);
        Category category2 = new Category("Fruchsäfte", true);
        testService.addCategory(new LinkedHashMap<>() {{put("name", "Lebensmittel"); put("vegan", "false");}});
        testService.addCategory(new LinkedHashMap<>() {{put("name", "Fruchtsäfte"); put("vegan", "true");}});

        Assertions.assertEquals(2, testService.getCategories(0, 5).length);
        testService.deleteCategory(category1.id);
        Assertions.assertEquals(1, testService.getCategories(0, 5).length);
        Assertions.assertEquals(category2, testService.getCategories(0, 2)[0]);
    }

}
