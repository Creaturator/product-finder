package ch.kuehle.joel.productfinder;

import ch.kuehle.joel.productfinder.model.City;
import ch.kuehle.joel.productfinder.model.Store;
import ch.kuehle.joel.productfinder.repository.CityRepository;
import ch.kuehle.joel.productfinder.repository.StoreRepository;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.client.RestTemplate;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
@AutoConfigureDataJpa
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.AUTO_CONFIGURED)
@Rollback
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class StoreControllerTest {

    @Autowired
    private MockMvc requestHandler;

    @Autowired
    private StoreRepository testRepo;

    @Autowired
    private CityRepository cityRepo;

    @BeforeEach
    public void setUp() {
        City andorra = cityRepo.findById(3041563).get();
        City dubai = cityRepo.findById(292223).get();
        testRepo.save(new Store("Rema 1000", "Fodseldagsgade 15", andorra));
        testRepo.save(new Store("Aldi Süd", "Bahnhofstraße 23", dubai));
        testRepo.save(new Store("Rewe", "Hauptstraße 68", dubai));
        testRepo.flush();
    }

    @AfterEach
    public void tearDown() {
        testRepo.deleteAll();
    }

    @Test
    public void testCreateStore() throws Exception {
        requestHandler.perform(MockMvcRequestBuilders.post("/api/store/create")
                        .content("{\"name\": \"Rewe\",\"address\": \"Hauptstraße 68\",\"city\": 1132344}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer " + obtainAccessToken()).with(SecurityMockMvcRequestPostProcessors.csrf()))
                .andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("true")));
    }

    @Test
    public void testUpdateStore() throws Exception {
        requestHandler.perform(MockMvcRequestBuilders.put("/api/store/update")
                .content("{\"name\": \"Kaufland\", \"id\": 3}").contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + obtainAccessToken()).with(SecurityMockMvcRequestPostProcessors.csrf()))
                .andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("true")));
    }

    @Test
    public void testGetNStores() throws Exception {
        requestHandler.perform(MockMvcRequestBuilders.get("api/store/fetch")
                        .queryParam("start", String.valueOf(1)).queryParam("amount", String.valueOf(2))
                .header("Authorization", "Bearer " + obtainAccessToken()).with(SecurityMockMvcRequestPostProcessors.csrf()))
                .andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("Rewe")))
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("Aldi Süd")));
    }

    @Test
    public void testDeleteStore() throws Exception {
        requestHandler.perform(MockMvcRequestBuilders.delete("/api/store/delete")
                .queryParam("id", String.valueOf(1))
                .header("Authorization", "Bearer " + obtainAccessToken()).with(SecurityMockMvcRequestPostProcessors.csrf()))
                .andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("true")));
    }

    private String obtainAccessToken() {
        RestTemplate rest = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        String body = "client_id=springAPI&" +
                "grant_type=password&" +
                "scope=openid profile roles offline_access&" +
                "username=admin&" +
                "password=admin";
        HttpEntity<String> entity = new HttpEntity<>(body, headers);
        ResponseEntity<String> resp = rest.postForEntity("http://localhost:8080/realms/Productfinder/protocol/openid-connect/token", entity, String.class);
        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(resp.getBody()).get("access_token").toString();
    }

}
