# Product Finder
This is a three component project which I worked on as three school assignments. It contains an Android app for adding receipts, a Spring backend for saving data and calculating the shop with the cheapest products and a js frontend.

City data provided by geonames.

To create the fitting enum data type for the nutri score, use the following webpage as help: https://www.postgresql.org/docs/current/datatype-enum.html